﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace bataillecafe
{
    class CommunicationServeur
    {
        /* Cette méthode permet de se connecter à un serveur (intitulé "endPoint"),
         * L'ip et le port sont est fixe (ici 51.91.120.237, port 1212). 
         * Elle créer ensuite un socket (intitulé "client) et fait appel a la fonction "InteractionServeur" 
         * afin de permettre la communication entre la machine et le serveur
         * 
         * Elle permet aussi d'afficher dans la console si une erreur est détecté lors de la connection*/
        public static void ConnectionServeur(Socket p_Serveur, IPEndPoint p_EndPoint)
        {
            try
            {
                try
                {
                    p_Serveur.Connect(p_EndPoint);
                }
                catch (SocketException e1) { Console.WriteLine("SocketException : {0}", e1.ToString()); }
                catch (Exception e3) { Console.WriteLine("Unexpected exception : {0}", e3.ToString()); }

            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }
        }

        public static void DeconnexionServeur(Socket p_Serveur)
        {
            try
            {
                try
                {
                    p_Serveur.Shutdown(SocketShutdown.Both);
                    p_Serveur.Close();
                }
                catch (SocketException e1) { Console.WriteLine("SocketException : {0}", e1.ToString()); }
                catch (Exception e3) { Console.WriteLine("Unexpected exception : {0}", e3.ToString()); }

            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }
        }

        /* Cette méthode permet de recevoir la trame que le serveur transmet, elle connecte le socket "client" passé en paramètre
         * Au serveur dont l'ip et le port aussi sont passé en paramètre (endPoint), une fois connecter, on stocker la trame 
         * Dans une chaine de caractère, puis fait apelle a la méthode "DecoupaGetrame",
         * Elle bloque ensuite les transmission et deconnecte le client du serveur.
         * 
         * INPUT -> [Socket client, IPEndPoint endPoint]
         * OUTPUT -> []
         */
        public static void RecevoirTrame(Socket p_Serveur)
        {
            byte[] messageReceived = new byte[1024]; // Data buffer    
            try
            {
                int byteRecv = p_Serveur.Receive(messageReceived);
                string Trame = Encoding.ASCII.GetString(messageReceived, 0, byteRecv);
                DecoupageTrame(Trame);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("FERMETURE DU PROGRAMME");
                Console.ReadKey();
                Environment.Exit(0);
            }



        }

        public static void EnvoieServeur(Socket p_Serveur, string p_Coordonnee)
        {
            byte[] v_MessageAEnvoye = Encoding.ASCII.GetBytes(p_Coordonnee);
            p_Serveur.Send(v_MessageAEnvoye);
        }

        public static string RecevoirServeur(Socket p_Serveur)
        {
            byte[] v_MessageRecuByte = new byte[1024];
            int v_ByteRec = p_Serveur.Receive(v_MessageRecuByte);
            string v_MessageRecuString = Encoding.ASCII.GetString(v_MessageRecuByte, 0, v_ByteRec);
            return v_MessageRecuString;
        }

        /* Cette méthode permet de découper la trame reçu selon un format précis, et de la stocker dans un tableau de string
         * Lorsque le caractère ":" est détecté dans la trame, l'index de colonne est incrémenté,
         * Lorsque le caractère "|" est détecté dans la trame, l'index de ligne est incrémenté,
         * Elle fait ensuite apelle a une fonction de convertion de String en Int afin de décoder la carte
         * Elle prend en paramètre la Trame émise par le serveur au client sous la forme d'une chaine de caractère,
         * 
         * INPUT -> [String Trame]
         * OUTPUT -> []
         */
        public static void DecoupageTrame(String Trame)
        {
            string[,] valeurUnite = new string[10, 10];
            int ligne = 0, colonne = 0;
            foreach (char carac in Trame)
            {
                if (carac == '|')
                {
                    ligne++;
                    colonne = 0;
                }
                if (carac == ':') colonne++;
                if (carac != ':' && carac != '|') valeurUnite[ligne, colonne] = valeurUnite[ligne, colonne] + carac;
            }
            ConversionStringInt(valeurUnite);
        }

        /* Cette méthode permet la convertion de la valeur de chaque unite en entier, cette dernier étant encore au format "string", 
         * On affecte aussi à chaque unité une "id", puis on fait apelle aux méthodes "DefinitionTypeAndFrontiere() et 
         * "DefinitionParcelle"
         * On passe donc en paramètre un tableau "valeurUniteString" de taille 10*10 contenant les valeur de chaque unite au format string
         
         * INPUT -> [String[,] valeurUniteString]
         * OUTPUT -> []
         */
        public static void ConversionStringInt(String[,] valeurUniteString)
        {
            int[,] valeurUniteInt = new int[10, 10];
            int idUnite = 0;
            for (int index1 = 0; index1 < 10; index1++)
            {
                for (int index2 = 0; index2 < 10; index2++)
                {
                    idUnite++;
                    valeurUniteInt[index1, index2] = Int32.Parse(valeurUniteString[index1, index2]);
                    DecodageIlot.DefinitionTypeAndFrontiere(valeurUniteInt[index1, index2], idUnite);
                }
            }
            for (int index = 0; index < 2; index++)
            {
                DecodageIlot.DefinitionParcelle();
            }
        }
    }
}