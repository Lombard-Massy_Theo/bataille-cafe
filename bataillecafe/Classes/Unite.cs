﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bataillecafe
{
    class Unite
    {
        private int type;//0 terre, 1 mer, 2 foret
        private bool arable,northBorder, southBorder, eastBorder, westBorder;
        private int id;
        private int idParcelle;
        private IA owned;

        public Unite()
        {
            this.id = -1;
            this.type = -1;
            this.arable = false;
            this.northBorder = false;
            this.southBorder = false;
            this.eastBorder = false;
            this.westBorder = false;
            this.idParcelle = -1;
            this.owned = null;
        }
        public Unite(int type, int id ,bool arable, bool northBorder, bool southBorder, bool eastBorder, bool westBorder, int parcelle, IA owned)
        {
            this.id = id;
            this.type = type;
            this.arable = arable;
            this.northBorder = northBorder;
            this.southBorder = southBorder;
            this.eastBorder = eastBorder;
            this.westBorder = westBorder;
            this.idParcelle = parcelle;
            this.owned = owned;
        }

        public void SetType(int type)
        {
            this.type = type;
        }
        public void SetArable(bool arable)
        {
            this.arable = arable;
        }
        public void SetBelongTo(int parcelle)
        {
            this.idParcelle = parcelle;
        }
        public void SetNorthBorder(bool northBorder)
        {
            this.northBorder = northBorder;
        }
        public void SetSouthBorder(bool southBorder)
        {
            this.southBorder = southBorder;
        }
        public void SetEastBorder(bool eastBorder)
        {
            this.eastBorder = eastBorder;
        }
        public void SetWestBorder(bool westBorder)
        {
            this.westBorder = westBorder;
        }
        public void SetId(int id)
        {
            this.id = id;
        }
        public void SetOwner(IA owned)
        {
            this.owned = owned;
        }
        public new int GetType()
        {
            return type;
        }
        public bool GetArable()
        {
            return arable;
        }
        public int GetBelongTo()
        {
            return idParcelle;
        }
        public bool GetNorthBorder()
        {
            return northBorder;
        }
        public bool GetSouthBorder()
        {
           return southBorder;
        }
        public bool GetEastBorder()
        {
            return eastBorder;
        }
        public bool GetWestBorder()
        {
           return westBorder;
        }
        public IA GetOwned()
        {
            return owned;
        }
        public int GetId()
        {
            return id;
        }
    }
}
