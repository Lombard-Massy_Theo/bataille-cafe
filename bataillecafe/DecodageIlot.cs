﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bataillecafe
{
    class DecodageIlot
    {
        public static List<Unite> unites = new List<Unite>();
        /* Cette méthode est apelle pour chaque unité, elle permet de décoder sa valeur afin de définir son type et ses frontières,
         * Les valeurs sont défini dans le sujet du projet,
         * Une fois la valeur décoder, elle fait appel au constructeur d'unite en lui passant en paramètre ses attributs d'id, de type et 
         * de frontières,
         * et ajoute l'unité a la liste unites
         * 
         * INPUT -> [int valeurUnite, int idUnite]
         * OUTPUT -> []
         */
        public static void DefinitionTypeAndFrontiere(int valeurUnite, int idUnite)
        {
            int type = 0;
            bool northBorder = false, southBorder = false, eastBorder = false, westBorder = false;
            if ((valeurUnite / 64) == 1) { type = 1; valeurUnite = valeurUnite - 64; }
            if ((valeurUnite / 32) == 1) { type = 2; valeurUnite = valeurUnite - 32; }
            if ((valeurUnite / 8) == 1) { valeurUnite = valeurUnite - 8; eastBorder = true; }
            if ((valeurUnite / 4) == 1) { valeurUnite = valeurUnite - 4; southBorder = true; }
            if ((valeurUnite / 2) == 1) { valeurUnite = valeurUnite - 2; westBorder = true; }
            if ((valeurUnite / 1) == 1) { valeurUnite = valeurUnite - 1; northBorder = true; }
            Unite uniteToCreate = new Unite(type, idUnite, true, northBorder, southBorder, eastBorder, westBorder, -1, null);
            unites.Add(uniteToCreate);

        }

        /* Cette méthode permet de mettre les unites adjacente qui ne possède pas de frontiere avec l'unite dans 
         * laquel nous sommes positioné, dans la meme parcelle que celle ci,
         * à condition que la parcelle adjacente ne soit assigner à aucune autre parcelle, 
         * elle prend en paramètre l'entier "compt", indicateur de l'unite dans la liste unites
         * 
         * INPUT -> [int compt]
         * OUTPUT ->[]
         */
        public static void DefinitionParcelleUniteAdjacente(int compt)
        {
            AffectationUniteEast(compt);
            AffectationUniteNorth(compt);
            AffectationUniteWest(compt);
            AffectationUnitSouth(compt);
        }

        /* Ces méthodes permettent d'assignées aux parcelles situé dans les 4 directions (nord, sud, est et ouest)
         * la même parcelle que l'unite dans laquel nous nous trouvons tant qu'aucune frontière ne les sépare 
         * elle prend en paramètre :
         * -> l'entier "compt", indicateur de l'unite dans la liste unites
         * 
         * INPUT -> [int compt]
         * OUTPUT ->[]
         */
        public static void AffectationUniteNorth(int compt)
        {
            int comptNorth = 1;

            if ((compt - (10 * comptNorth)) >= 0)
            {
                if (compt / 10 == 1 && unites[compt - (10 * comptNorth)].GetSouthBorder() == false) unites[compt - (10 * comptNorth)].SetBelongTo(unites[compt].GetBelongTo());
                while (unites[compt - (10 * comptNorth)].GetSouthBorder() == false && comptNorth < compt / 10)
                {
                    unites[compt - (10 * comptNorth)].SetBelongTo(unites[compt].GetBelongTo());
                    comptNorth++;
                }
            }
        }
        public static void AffectationUniteEast(int compt)
        {
            int comptEast = 1;
            if (compt + comptEast < 99)
            {
                if (99 - compt == 1 && unites[compt].GetEastBorder() == false) unites[compt + 1].SetBelongTo(unites[compt].GetBelongTo());
                while (unites[compt + comptEast].GetWestBorder() == false && comptEast < (99 - compt))
                {
                    unites[compt + comptEast].SetBelongTo(unites[compt].GetBelongTo());
                    comptEast++;
                }
            }
        }
        public static void AffectationUnitSouth(int compt)
        {
            int comptSouth = 1;
            if (compt + 10 <= 99)
            {
                if ((99 - compt) / 10 == 1 && unites[compt + 10].GetNorthBorder() == false) unites[compt + 10].SetBelongTo(unites[compt].GetBelongTo());
                while (unites[compt + (10 * comptSouth)].GetNorthBorder() == false && comptSouth < (99 - compt) / 10)//erreur
                {
                    unites[compt + (10 * comptSouth)].SetBelongTo(unites[compt].GetBelongTo());
                    comptSouth++;
                }
            }
        }
        public static void AffectationUniteWest(int compt)
        {
            int comptWest = 1;
            if ((compt - comptWest) >= 0)
            {
                while (unites[compt - comptWest].GetEastBorder() == false && comptWest > compt)
                {
                    unites[compt - comptWest].SetBelongTo(unites[compt].GetBelongTo());
                    comptWest++;
                }
            }
        }

        /* Cette méthode permet de vérifier que les unites situé sur la meme parcelle que l'unite sur 
         * laquel nous sommes positioné, ne possède pas déja un identifiant de parcelle; 
         * elle prend en paramètre l'entier "compt", indicateur de l'unite dans la liste unites
         * 
         * INPUT -> [int compt]
         * OUTPUT ->[]
         */
        public static void VerificationUniteNorth(int compt)
        {
            int comptNorth = 0;

            while (unites[compt - (comptNorth * 10)].GetNorthBorder() == false && unites[compt].GetBelongTo() == -1)
            {
                if (unites[compt - (comptNorth * 10)].GetBelongTo() == -1) comptNorth++;
                else unites[compt].SetBelongTo(unites[compt - (comptNorth * 10)].GetBelongTo());
            }
        }
        public static void VerificationUniteEast(int compt)
        {
            int comptEast = 0;
            if (compt < 99)
            {
                if (unites[compt + 1].GetEastBorder() == true && unites[compt + 1].GetWestBorder() == false) unites[compt].SetBelongTo(unites[compt + 1].GetBelongTo());
                else
                {
                    while (unites[compt + comptEast].GetEastBorder() == false && unites[compt].GetBelongTo() == -1)
                    {
                        if (unites[compt + comptEast].GetBelongTo() == -1) comptEast++;
                        else unites[compt].SetBelongTo(unites[compt + comptEast].GetBelongTo());
                    }
                }
            }
        }
        public static void VerificationUnitSouth(int compt)
        {
            int comptSouth = 0;
            while (unites[compt + (comptSouth * 10)].GetSouthBorder() == false && unites[compt].GetBelongTo() == -1)
            {
                if (unites[compt + (comptSouth * 10)].GetBelongTo() == -1) comptSouth++;
                else unites[compt].SetBelongTo(unites[compt + (comptSouth * 10)].GetBelongTo());
            }
        }
        public static void VerificationUniteWest(int compt)
        {
            int comptWest = 0;
            while (unites[compt - comptWest].GetWestBorder() == false && unites[compt].GetBelongTo() == -1)
            {
                if (unites[compt - comptWest].GetBelongTo() == -1) comptWest++;
                else unites[compt].SetBelongTo(unites[compt - comptWest].GetBelongTo());
            }
        }

        /* Cette méthode permet de créer de creer une nouvelle parcelle en incrémentant "idParcelle" lorsque aucune des unitées de la parcelle 
         * ne possède didentifiant de parcelle;
         * Elle prend en paramètre :
         * -> l'entier "compt", indicateur de l'unite dans la liste unites
         * -> l'entier "idParcelle", passe par référence, permettant d'incrémenter l'index de parcelle
         * 
         * INPUT -> [int compt, ref int idParcelle]
         * OUTPUT ->[]
         */
        public static void ParcelleInconnu(int compt, ref int idParcelle)
        {
            int index = 0;
            bool formeParcelleSpecial = false;
            if (unites[compt].GetBelongTo() == -1)
            {
                while (unites[compt + (index * 10)].GetSouthBorder() == false && index < (99 - compt) / 10)
                {
                    if (unites[compt + (index * 10)].GetSouthBorder() == false && unites[compt + (index * 10)].GetWestBorder() == false) formeParcelleSpecial = true;
                    if (unites[compt + ((index + 1) * 10)].GetSouthBorder() == true && unites[compt + ((index + 1) * 10)].GetWestBorder() == false &&
                        unites[compt + ((index + 1) * 10)].GetEastBorder()) formeParcelleSpecial = true;
                    index++;
                }
                if (compt > 1)
                {
                    if (unites[compt - 1].GetEastBorder() == false && unites[compt + 10].GetBelongTo() == -1) formeParcelleSpecial = true;
                }
                if (formeParcelleSpecial == true) unites[compt].SetBelongTo(-1);
                else
                {
                    unites[compt].SetBelongTo(idParcelle);
                    idParcelle++;
                }
            }
        }

        /* Cette méthode permet d'assigne une parcelle a l'unite dans laquel nous nous trouvons, si cette dernière n'en possède pas
         * Elle prend en paramètre :
         * -> l'entier "compt", indicateur de l'unite dans la liste unites
         * -> l'entier "idParcelle", passe par référence, permettant d'incrémenter l'index de parcelle
         * 
         * INPUT -> [int compt, ref int idParcelle]
         * OUTPUT ->[]
         */
        public static void DefinitionParcelleUniteCentrale(int compt, ref int idParcelle)
        {

            VerificationUniteEast(compt);
            VerificationUniteNorth(compt);
            VerificationUniteWest(compt);
            VerificationUnitSouth(compt);
            ParcelleInconnu(compt, ref idParcelle);
        }

        /* Cette méthode permet de définir les 17 parcelles qui composent un ilot,
         * Elle fait apelle aux méthodes "DefinitionParcelleUniteCentrale" et "DefinitionParcelleUniteAdjacente"
         * Elle ne prend aucune valeur en entrée 
         * 
         * INPUT -> []
         * OUTPUT ->[]
         */
        public static void DefinitionParcelle()
        {
            int idParcelle = 1;
            for (int compt = 0; compt < 100; compt++)
            {
                if (unites[compt].GetType() == 0)
                {
                    if (unites[compt].GetBelongTo() == -1)
                    {
                        DefinitionParcelleUniteCentrale(compt, ref idParcelle);
                    }
                    else
                    {
                        if (unites[compt].GetWestBorder() == false && unites[compt - 1].GetBelongTo() != unites[compt].GetBelongTo()
                            && unites[compt - 1].GetBelongTo() != -1)
                        {
                            unites[compt].SetBelongTo(unites[compt - 1].GetBelongTo());
                        }
                        else if (unites[compt].GetNorthBorder() == false && unites[compt - 10].GetBelongTo() != unites[compt].GetBelongTo()
                            && unites[compt - 10].GetBelongTo() != -1)
                        {
                            unites[compt].SetBelongTo(unites[compt - 10].GetBelongTo());
                        }
                    }
                    DefinitionParcelleUniteAdjacente(compt);
                }
            }
        }

        // Cette méthode permet d'afficher l'ilot dans la console
        public static void AffichageIlot()
        {
            for (int compt = 0; compt < 100; compt++)
            {
                if (unites[compt].GetType() == 0)
                {
                    Console.Write((char)(unites[compt].GetBelongTo() + 96) + " ");
                    var test = unites[compt].GetBelongTo();
                }
                if (unites[compt].GetType() == 1)
                {
                    Console.Write("M ");
                }
                if (unites[compt].GetType() == 2)
                {
                    Console.Write("F ");
                }
                if ((compt + 1) % 10 == 0)
                {
                    Console.WriteLine();
                }
            }
        }
        //Cette méthode va permettre de récupérer l'ilot jusque là convertit en tableau 2 dimensions avec des chiffres à l'intérieur
        //Nous allons donc pouvoir en fonction du chiffre detecté le transformer en un caractère pour au final obtenir la carte de jeu

        public static char[,] ObtenirIlot()
        {
            char[,] v_Ilot = new char[10, 10];
            int i = 0;
            int j = 0;
            for (int compt = 0; compt < 100; compt++)
            {

                if (unites[compt].GetType() == 0)
                {
                    v_Ilot[i, j] = Convert.ToChar((unites[compt].GetBelongTo() - 1 + 'a'));
                }
                if (unites[compt].GetType() == 1)
                {
                    v_Ilot[i, j] = 'M';
                }
                if (unites[compt].GetType() == 2)
                {
                    v_Ilot[i, j] = 'F';
                }
                if ((compt + 1) % 10 == 0)
                {
                    i++;
                    j = 0;
                }
                else
                {
                    j++;
                }

            }

            return v_Ilot;
        }
    }
}