﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace bataillecafe
{
    class Program
    {
        public static void Main(string[] args)
        {
            //On se connecte au serveur avec l'aide d'un socket
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1213);
            Socket Serveur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            CommunicationServeur.ConnectionServeur(Serveur, endPoint);
            CommunicationServeur.RecevoirTrame(Serveur);
            DecodageIlot.AffichageIlot();
            char[,] v_ObjetIlotMAJ = DecodageIlot.ObtenirIlot();
            char[,] v_ObjetIlot = DecodageIlot.ObtenirIlot();
            string v_ScoreFinale = "";
            int[] tab = new int[100];
            int a = 1;
            foreach (Unite u in DecodageIlot.unites)
            {
                if (u.GetBelongTo() > -1) tab[u.GetBelongTo()]++;
            }
            for (int i = 0; i < 100; i++)
            {
                if (tab[i] != 0)
                {
                    Console.WriteLine(a + ":" + tab[i]);
                    a++;
                }
            }

            int EtatParti = 0;

            Console.WriteLine("Tapez 'M' pour jouer manuellement , et 'A' pour le mode automatique (IA)");
            char choix = Console.ReadKey().KeyChar;
            string v_XY = null;
            if (choix == 'M')
            {
                #region Gestion jeux manuel
                do
                {
                    AffichageIlot(v_ObjetIlot); //On commence par récupérer notre ilot
                    AffichageIlot(v_ObjetIlotMAJ);

                    Console.WriteLine("Joueur A : Quel case voulez vous planter? (Ecrivez les coordonnées XY)");
                    do
                    {
                        v_XY = Console.ReadLine();
                    } while (v_XY == null || v_XY == ""); //On ecrit une chaine du type 25 par exemple

                    string v_Coordonnee = "A:" + v_XY; 
                    string v_CoordonneeX = v_Coordonnee.Substring(2, 1);
                    string v_CoordonneeY = v_Coordonnee.Substring(3, 1);

                    // Jouer un coup
                    CommunicationServeur.EnvoieServeur(Serveur, v_Coordonnee);

                    string v_Reponse1 = CommunicationServeur.RecevoirServeur(Serveur);
                    string v_Reponse2 = CommunicationServeur.RecevoirServeur(Serveur);
                    string v_Reponse3 = CommunicationServeur.RecevoirServeur(Serveur);


                    if (v_Reponse1 == "VALI")// Si le coup correspond aux données saisis plus haut alors on l'accepte
                    {
                        Console.WriteLine("Votre coup est valide!");
                        v_ObjetIlotMAJ[Convert.ToInt32(v_CoordonneeX), Convert.ToInt32(v_CoordonneeY)] = 'X';
                    }
                    else if (v_Reponse1 == "INVA") // Dans le cas inverse on le refuse par exemple si 1 ou 125 ou 1B le coup est refusé
                    {
                        Console.WriteLine("Votre coup est invalide!");
                    }
                    else
                    {
                        Console.WriteLine("ERREUR");
                    }


                    if (v_Reponse2 == "FINI")
                    {
                        Console.WriteLine("La joueur B a FINI");
                    }
                    else
                    {
                        Console.WriteLine("Le joueur B a joué : " + v_Reponse2.Substring(2, 2));
                        string v_BCoordonneeX = v_Reponse2.Substring(2, 1);
                        string v_BCoordonneeY = v_Reponse2.Substring(3, 1);

                        v_ObjetIlotMAJ[Convert.ToInt32(v_BCoordonneeX), Convert.ToInt32(v_BCoordonneeY)] = 'O'; //On convertit les coordonnées saisis qu'on modélisera par un O dans la carte
                    }

                    if (v_Reponse3 != "ENCO")
                    {
                        Console.WriteLine("La partie est terminé!");
                        EtatParti = 1;
                    }
                    Thread.Sleep(1000);
                }
                while (EtatParti != 1);
                #endregion
            }
            else if (choix == 'A')
            {
                #region Gestion jeux automatique
                //La partie IA reprend majoritairement les grandes lignes de celle Manuel sauf qu'on appelle la classe IA pour chaque action que le serveur doit effectuer
                string v_Coordonnee = "AUCUN";
                string v_dernierCoupAdverse = null;
                string v_dernierCoupClient = null;
                do
                {

                    AffichageIlot(v_ObjetIlot);
                    AffichageIlot(v_ObjetIlotMAJ);

                    if (v_Coordonnee == "AUCUN")
                    {
                        v_Coordonnee = IntelligenceArtificielle.ObtenirCoordPremierCoup(v_ObjetIlotMAJ);
                    }
                    else
                    {
                        Thread.Sleep(3000);
                        v_Coordonnee = IntelligenceArtificielle.ObtenirCoordCoup(v_ObjetIlot, v_ObjetIlotMAJ, v_dernierCoupAdverse, v_dernierCoupClient);
                    }

                    CommunicationServeur.EnvoieServeur(Serveur, v_Coordonnee);

                    string v_CoordonneeX = v_Coordonnee.Substring(2, 1);
                    string v_CoordonneeY = v_Coordonnee.Substring(3, 1);
                    v_dernierCoupClient = v_CoordonneeX + v_CoordonneeY;

                    Console.WriteLine("L'IA a joué : " + v_CoordonneeX + v_CoordonneeY);

                    string v_Reponse1 = CommunicationServeur.RecevoirServeur(Serveur);
                    string v_Reponse2 = CommunicationServeur.RecevoirServeur(Serveur);
                    string v_Reponse3 = CommunicationServeur.RecevoirServeur(Serveur);

                    if (v_Reponse1 == "VALI")
                    {
                        Console.WriteLine("Le coup a été validé par le serveur!");
                        v_ObjetIlotMAJ[Convert.ToInt32(v_CoordonneeX), Convert.ToInt32(v_CoordonneeY)] = 'X';
                    }
                    else if (v_Reponse1 == "INVA")
                    {
                        Console.WriteLine("Le coup n'est pas valide!");
                    }
                    else
                    {
                        Console.WriteLine("ERREUR");
                    }


                    if (v_Reponse2 == "FINI")
                    {
                        Console.WriteLine("La joueur B a FINI");
                    }
                    else
                    {
                        Console.WriteLine("Le joueur B a joué : " + v_Reponse2.Substring(2, 2));
                        string v_BCoordonneeX = v_Reponse2.Substring(2, 1);
                        string v_BCoordonneeY = v_Reponse2.Substring(3, 1);

                        v_ObjetIlotMAJ[Convert.ToInt32(v_BCoordonneeX), Convert.ToInt32(v_BCoordonneeY)] = 'O';
                        v_dernierCoupAdverse = v_BCoordonneeX + v_BCoordonneeY;
                    }

                    if (v_Reponse3 != "ENCO")
                    {
                        Console.WriteLine("La partie est terminé!");
                        EtatParti = 1;
                        v_ScoreFinale = CommunicationServeur.RecevoirServeur(Serveur);
                    }


                } while (EtatParti != 1);
                #endregion
            }
            else
            {
                Console.WriteLine("Mauvaise touche!");
            }



            if (v_ScoreFinale == "")
            {

                Console.WriteLine("Erreur : Le score finale n'a pas été correctement recu.");
            }
            else
            {
                string v_ScoreClient = v_ScoreFinale.Substring(2, 2);
                string v_ScoreServeur = v_ScoreFinale.Substring(5, 2);
                Console.WriteLine("Le joueur A (client) a eu " + v_ScoreClient + " points et le joueur B (serveur) a eu " + v_ScoreServeur + " points.");
                //On affiche le score du client 
            }

            Console.WriteLine("Appuyez pour quittez");
            Console.ReadKey();

            CommunicationServeur.DeconnexionServeur(Serveur); //On se déconnecte proprement du serveur
            Environment.Exit(0);

        }






        public static void AffichageIlot(char[,] p_ObjetIlot) //On affiche notre carte d'ilot avec des délimitations pour un plus beau rendu
        {
            Console.WriteLine("--------------------------");
            Console.WriteLine("  0|1|2|3|4|5|6|7|8|9");
            Print2DArray(p_ObjetIlot);
            Console.WriteLine("--------------------------");
        }

        public static void Print2DArray<T>(T[,] matrix) //Rejoint notre fonction affichage ilot pour le rendu
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                Console.Write(i + "|");
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public static void RechercherCoupValide(char[,] p_ObjetIlot)
        {

        }
    }
}