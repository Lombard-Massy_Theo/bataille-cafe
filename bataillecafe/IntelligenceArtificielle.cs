﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace bataillecafe
{
    class IntelligenceArtificielle
    {

        //POur cette fonction on joue le 1er coup sur la ligne ou il y'a le plus de lettre du même type pour prendre un max de pts
        public static string ObtenirCoordPremierCoup(char[,] p_ObjetIlot)
        {
            string v_Ligne;

            char v_CaracterePlusSouvent = 'Z';
            int v_NbCaractere = 0;
            int v_LigneCaractere = 0;
            for (int v = 1; v < 9; v++)
            {

                v_Ligne = ObtenirLigne(p_ObjetIlot, v);

                for (int i = 0; i < 127; i++)
                {
                    if ((i != (char)0) && (i != (char)77) && (i != (char)88) && (i != (char)70) && (i != (char)79)) // On exclu M, 0 et X
                    {
                        int v_countCaractere = v_Ligne.ToCharArray().Count(c => c == i);

                        if (v_NbCaractere < v_countCaractere)
                        {
                            v_NbCaractere = v_countCaractere;
                            v_CaracterePlusSouvent = (char)i;
                            v_LigneCaractere = v;
                        }
                    }

                }
            }
            string v_Coord = TrouverCoordCaractereLigne(p_ObjetIlot, v_CaracterePlusSouvent, v_LigneCaractere);

            return v_Coord;

        }

        public static string ObtenirLigne(char[,] p_ObjetIlot, int p_NumeroLigne) //Fonction qui sera appelé quand on voudra trouver la ligne utilisée par le serveur pour prendre la meme
        {
            string v_Ligne = null;
            for (int j = 0; j < 10; j++)
            {
                v_Ligne = v_Ligne + p_ObjetIlot[p_NumeroLigne, j];

            }
            return v_Ligne;
        }

        public static string ObtenirColonne(char[,] p_ObjetIlot, int p_NumeroColonne) // Fonction qui sera appelée quand on voudra trouver la colonne et ensuite planter la graine 
        {
            string v_Colonne = null;
            for (int j = 0; j < 10; j++)
            {
                v_Colonne = v_Colonne + p_ObjetIlot[j, p_NumeroColonne];

            }
            return v_Colonne;
        }

        //Fonction qui récupere la coordonnée de la ligne quand il ya un X ou O pour ensuite dire quel coup a joue l IA (sur la ligne)
        public static string TrouverCoordCaractereLigne(char[,] p_ObjetIlot, char p_CaractereChercher, int p_NumeroLigne) 
        {
            int i = 0;
            char v_CaractereLu;
            do
            {
                do
                {
                    i++;
                    v_CaractereLu = p_ObjetIlot[p_NumeroLigne, i];

                } while (v_CaractereLu != p_CaractereChercher);
            } while (p_ObjetIlot[p_NumeroLigne, i] == 'X' || p_ObjetIlot[p_NumeroLigne, i] == 'O');

            return "A:" + p_NumeroLigne + i;
        }
        //Meme chose mais pour la colonne
        public static string TrouverCoordCaractereColonne(char[,] p_ObjetIlot, char p_CaractereChercher, int p_NumeroColonne)
        {
            int i = 0;
            char v_CaractereLu;
            do
            {
                do
                {
                    i++;
                    v_CaractereLu = p_ObjetIlot[i, p_NumeroColonne];

                } while (v_CaractereLu != p_CaractereChercher);

            } while (p_ObjetIlot[i, p_NumeroColonne] == 'X' || p_ObjetIlot[i, p_NumeroColonne] == 'O');

            return "A:" + i + (p_NumeroColonne);
        }

        public static string ObtenirCoordCoup(char[,] p_ObjetIlot, char[,] p_ObjetIlotMAJ, string p_DernierCoupAdverse, string p_DernierCoupClient)
        {
            // Obtention des infos du dernier coup adverse
            string v_DernierCoupAdverseCoordonneeX = p_DernierCoupAdverse.Substring(0, 1);
            string v_DernierCoupAdverseCoordonneeY = p_DernierCoupAdverse.Substring(1, 1);
            char v_DernierCoupAdverseCaractere = p_ObjetIlot[Convert.ToInt32(v_DernierCoupAdverseCoordonneeX), Convert.ToInt32(v_DernierCoupAdverseCoordonneeY)];

            // Obtention des infos du dernier coup client
            string v_DernierCoupClientCoordonneeX = p_DernierCoupClient.Substring(0, 1);
            string v_DernierCoupClientCoordonneeY = p_DernierCoupClient.Substring(1, 1);
            char v_DernierCoupClientCaractere = p_ObjetIlot[Convert.ToInt32(v_DernierCoupClientCoordonneeX), Convert.ToInt32(v_DernierCoupClientCoordonneeY)];

            // On essaye de prendre une case a droite de celle que l'on a déja pris
            if ((p_ObjetIlot[Convert.ToInt32(v_DernierCoupClientCoordonneeX), Convert.ToInt32(v_DernierCoupClientCoordonneeY) + 1] == v_DernierCoupClientCaractere)
                && (p_ObjetIlotMAJ[Convert.ToInt32(v_DernierCoupClientCoordonneeX), Convert.ToInt32(v_DernierCoupClientCoordonneeY) + 1] != 'O')
                && (p_ObjetIlotMAJ[Convert.ToInt32(v_DernierCoupClientCoordonneeX), Convert.ToInt32(v_DernierCoupClientCoordonneeY) + 1] != 'X')
                && (v_DernierCoupAdverseCoordonneeX == v_DernierCoupClientCoordonneeX)// Si l'IA n'a pas changé de ligne
                )
            {
                return "A:" + v_DernierCoupClientCoordonneeX + (Convert.ToInt32(v_DernierCoupClientCoordonneeY) + 1);
            }
            else
            {
                string v_Ligne = ObtenirLigne(p_ObjetIlotMAJ, Convert.ToInt32(v_DernierCoupAdverseCoordonneeX));
                int v_NbCaractere = 0;
                char v_CaracterePlusSouvent = '?';
                for (int i = 0; i < 127; i++)
                {
                    if ((i != (char)0) && (i != (char)77) && (i != (char)88) && (i != (char)79) && (i != (char)70) && (i != (char)v_DernierCoupAdverseCaractere)) // On exclu M, 0 et X
                    {
                        int v_countCaractere = v_Ligne.ToCharArray().Count(c => c == i);

                        if (v_NbCaractere < v_countCaractere)
                        {
                            v_NbCaractere = v_countCaractere;
                            v_CaracterePlusSouvent = (char)i;
                        }
                    }
                }
                if (v_CaracterePlusSouvent != '?')
                {
                    string v_Coord = TrouverCoordCaractereLigne(p_ObjetIlotMAJ, v_CaracterePlusSouvent, Convert.ToInt32(v_DernierCoupAdverseCoordonneeX));
                    return v_Coord;
                }
                else // CHANGEMENT DE LIGNE
                {
                    string v_Colonne = ObtenirColonne(p_ObjetIlotMAJ, Convert.ToInt32(v_DernierCoupAdverseCoordonneeY));
                    int v_NbCaractere2 = 0;
                    char v_CaracterePlusSouvent2 = '?';
                    for (int i = 0; i < 127; i++)
                    {
                        if ((i != (char)0) && (i != (char)77) && (i != (char)88) && (i != (char)79) && (i != (char)70) && (i != (char)v_DernierCoupAdverseCaractere)) // On exclu null, M, 0, X et le caractere du dernier coup
                        {
                            int v_countCaractere = v_Colonne.ToCharArray().Count(c => c == i);

                            if (v_NbCaractere2 < v_countCaractere)
                            {
                                v_NbCaractere2 = v_countCaractere;
                                v_CaracterePlusSouvent2 = (char)i;
                            }
                        }
                    }
                    if (v_CaracterePlusSouvent2 != '?')
                    {
                        string v_Coord = TrouverCoordCaractereColonne(p_ObjetIlotMAJ, v_CaracterePlusSouvent2, Convert.ToInt32(v_DernierCoupAdverseCoordonneeY));
                        return v_Coord;
                    }
                }
            }





            // On regarde d'abord sur la ligne
            return "ERREUR";// INCORRECT

        }


    }
}